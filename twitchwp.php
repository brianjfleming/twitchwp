<?php
/**
 * Use the PHP for https://bitbucket.org/brianjfleming/twitchwp
 *
 * @package   TwitchWP
 * @author    Brian Fleming <meandwooster@gmail.com>, Bob Whitis
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2017 WTF Tech
 *
 * @wordpress-plugin
 * Plugin Name:       TwitchWP
 * Plugin URI:        https://bitbucket.org/brianjfleming/twitchwp/overview
 * Description:       Seamless integration support between Twitch.tv and WordPress
 * Version:           1.0.0
 * Author:            WTF Technologies
 * Author URI:        http://www.slimbobwp.com
 * Text Domain:       twitch-wp
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 * GitHub Plugin URI: https://bitbucket.org/brianjfleming/twitchwp
 */

namespace TwitchWP;

// Global Instances
global $twitchwp_ui,       // Holds our Admin UI Class
       $twitchwp_link,     // Holds our API Link Stream and Data
       $twitchwp_public;   // Holds our Public Methods

// Global Constants
define( 'TWITCHWP_URL', plugin_dir_url( __FILE__ ) );
define( 'TWITCHWP_PATH', dirname( __FILE__ ) . '/' );
define( 'TWITCHWP_PUBLIC_PATH', TWITCHWP_PATH . '/public/' );
define( 'TWITCHWP_ADMIN_PATH', TWITCHWP_PATH . '/admin/' );
define( 'TWITCHWP_WIDGETS_PATH', TWITCHWP_PATH . '/widgets/' );

// Widget Loads
require_once( TWITCHWP_WIDGETS_PATH . 'load.php' );

function twitchwp_setup() {

	do_action( 'twitch_wp_before_setup' );

	// Asset Defines
	define( 'TWITCHWP_LOGO', TWITCHWP_URL . 'assets/images/twitchwp.png' );

	// Admin Queues
	if ( is_admin() ) {

		require_once( TWITCHWP_ADMIN_PATH . 'class-twitchwp-ui.php' );      // Admin UI Class
		require_once( TWITCHWP_ADMIN_PATH . 'class-twitchwp-link.php' );    // Twitch API Link Class

		$twitchwp_ui   = new \TwitchWP_UI();
		$twitchwp_link = new \TwitchWP_Link();

	}

	// Frontend Queues
	if ( ! is_admin() ) {

		require_once( TWITCHWP_PUBLIC_PATH . 'class-twitchwp-public.php' );    // Twitch API Link Class

		$twitchwp_public = new \TwitchWP_Public();

	}

	do_action( 'twitch_wp_after_setup' );

}

add_action( 'init', __NAMESPACE__ . '\twitchwp_setup', 10 );