<?php

class TwitchWP_Public {

	function __construct() {
		$this->twitchwp_public_actions();
		$this->twitchwp_public_filters();
	}

	function twitchwp_public_actions() {
		add_action( 'wp_enqueue_scripts', array( $this, 'twitchwp_public_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'twitchwp_public_scripts' ) );

		do_action( 'twitchwp_public_actions' );
	}

	function twitchwp_public_filters() {

	}

	function twitchwp_public_styles() {

		do_action( 'twitchwp_public_styles_loaded' );
	}

	function twitchwp_public_scripts() {
		wp_register_script( 'twitchwp_twitch_embed', 'http://player.twitch.tv/js/embed/v1.js' );
		wp_enqueue_script( 'twitchwp_twitch_embed' );

		do_action( 'twitchwp_public_scripts_loaded' );
	}

}