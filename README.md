# TwitchWP #
**Contributors:**      Brian Fleming, Bob Whitis  
**Donate link:**       www.lumberjackweb.com  
Tags: 
**Requires at least:** 4.7.2  
**Tested up to:**      4.7.2  
**Stable tag:**        1.0.0  
**License:**           GPLv2 or later  
**License URI:**       http://www.gnu.org/licenses/gpl-2.0.html  

Adds support for Locations 


### NOTES ###

* May not need the partitioning of `class-twitchwp-ui`
* Add OAuth from twitch to Channels CPT for validation
* Test OAuth token: `x9ukywmmdjjpctluhenf8f4b3qymn0`

### 0.1.0 ###
* Plugin Base Files
