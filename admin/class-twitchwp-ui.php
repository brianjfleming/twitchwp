<?php

class TwitchWP_UI {

	private $admin_capability = 'manage_options';

	function __construct() {
		$this->twitchwp_ui_actions();
		$this->twitchwp_ui_filters();
	}

	function twitchwp_ui_actions() {
		add_action( 'admin_menu', array( $this, 'twitchwp_ui_menu' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'twitchwp_ui_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'twitchwp_ui_scripts' ) );
	}

	function twitchwp_ui_filters() {

	}

	function twitchwp_ui_styles() {
		wp_register_style( 'twitchwp_styles', TWITCHWP_URL . 'assets/css/twitchwp.min.css' );
		wp_enqueue_style( 'twitchwp_styles' );
	}

	function twitchwp_ui_scripts() {
		wp_register_script( 'twitchwp_scripts', TWITCHWP_URL . 'assets/js/twitchwp.min.js' );
		wp_enqueue_script( 'twitchwp_scripts' );

		wp_register_script( 'twitchwp_fontawesome', 'https://use.fontawesome.com/f3b85f6fdf.js' );
		wp_enqueue_script( 'twitchwp_fontawesome' );
	}

	function twitchwp_ui_menu() {
		add_menu_page( 'TwitchWP', 'TwitchWP', $this->admin_capability, 'twitchwp', array(
			$this,
			'twitchwp_ui_master'
		), 'none', 9 );
		add_submenu_page( 'twitchwp', 'Channels < TwitchWP', 'Channels', $this->admin_capability, 'twitchwp-channels', array(
			$this,
			'twitchwp_ui_channels'
		) );
	}

	function twitchwp_ui_master() {

		echo '<div class="wrap">';
		echo '<h1 class="wp-heading-inline">TwitchWP</h1>';
		echo '<div class="postbox"><div class="inside">Master Page</div></div>';
		echo '</div>';

	}

	function twitchwp_ui_channels() {

		echo '<div class="wrap">';
		echo '<h1 class="wp-heading-inline">Channels < TwitchWP</h1>';
		echo '<div class="postbox"><div class="inside">Channels Page</div></div>';
		echo '</div>';

	}

	function twitchwp_has_permissions( $die = true ) {
		if ( ! current_user_can( $this->admin_capability ) ) {

			if ( $die ) {
				wp_die( __( 'Sorry, but you do not have permissions to view this page' ) );
			} else {
				return false;
			}

		} else {
			return true;
		}
	}

}