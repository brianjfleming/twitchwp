<?php

namespace TwitchWP\Widgets;

require_once( TWITCHWP_WIDGETS_PATH . 'class-widget-twitchwp-player.php' );
require_once( TWITCHWP_WIDGETS_PATH . 'class-widget-twitchwp-chat.php' );


/**
 * Registers the widget
 * @return bool
 */
function register_twitch_widgets() {
	register_widget( 'TwitchWP\Widgets\TwitchWP_Player_Widget' );
	register_widget( 'TwitchWP\Widgets\TwitchWP_Chat_Widget' );
}
add_action( 'widgets_init', __NAMESPACE__ . '\register_twitch_widgets' );