<?php

namespace TwitchWP\Widgets;

class TwitchWP_Player_Widget extends \WP_Widget {

	// Defaults
	public $defaults = array(
		'title'         => '',
		'stream_id'     => '',
		'player_width'  => '',
		'player_height' => ''
	);

	function __construct() {

		// Setup Parent Globals, etc
		$widget_ops = array(
			'classname'                   => 'twitchwp-player-widget',
			'description'                 => __( 'Embeds the livestream Twitch Player for a selected stream' ),
			'customize_selective_refresh' => true,
		);

		parent::__construct( 'twitchwp_player_widget', __( 'Twitch Player' ), $widget_ops );
	}

	/**
	 * Widget: Output our widget
	 *
	 * @param array $args
	 * @param array $instance
	 *
	 * @return bool
	 */
	public function widget( $args, $instance ) {

		$instance = wp_parse_args( $instance, $this->defaults );
		$title    = apply_filters( 'widget_title', $instance['title'] );

		// BEFORE WIDGET
		echo $args['before_widget'];

		// TITLE
		if ( ! empty( $instance['title'] ) ) {
			echo $args['before_title'] . do_shortcode( $title ) . $args['after_title'];
		}

		// OUTPUT
		echo '<div id="twitchwp-' . $instance['stream_id'] . '-player"></div>';
		echo '<script type="text/javascript">var options = { width: ' . $instance['player_width'] . ', height: ' . $instance['player_height'] . ', channel: "' . strtolower( $instance['stream_id'] ) . '" };';
		echo 'var player = new Twitch.Player("twitchwp-' . $instance['stream_id'] . '-player", options);';
		echo 'player.setVolume(0.5);';
		echo '</script>';

		// AFTER WIDGET
		echo $args['after_widget'];

		return true;
	}

	/**
	 * Form: Output our widget options
	 *
	 * @param array $instance
	 *
	 * @return bool
	 */
	public function form( $instance ) {

		$instance = wp_parse_args( $instance, $this->defaults );

		// TITLE
		echo '<p>';
		echo '<label for="' . $this->get_field_name( 'title' ) . '">';
		echo 'Title:';
		echo '<input class="widefat" id="' . $this->get_field_id( 'title' ) . '" name="' . $this->get_field_name( 'title' ) . '" type="text" value="' . esc_attr( $instance['title'] ) . '">';
		echo '</label>';
		echo '</p>';

		// STREAM ID
		echo '<p>';
		echo '<label for="' . $this->get_field_name( 'stream_id' ) . '">';
		echo 'Steam ID:';
		echo '<input class="widefat" id="' . $this->get_field_id( 'stream_id' ) . '" name="' . $this->get_field_name( 'stream_id' ) . '" type="text" value="' . esc_attr( $instance['stream_id'] ) . '">';
		echo '</label>';
		echo '</p>';

		// PLAYER WIDTH
		echo '<p>';
		echo '<label for="' . $this->get_field_name( 'player_width' ) . '">';
		echo 'Width:';
		echo '<input class="widefat" id="' . $this->get_field_id( 'player_width' ) . '" name="' . $this->get_field_name( 'player_width' ) . '" type="text" value="' . esc_attr( $instance['player_width'] ) . '">';
		echo '</label>';
		echo '</p>';

		// PLAYER HEIGHT
		echo '<p>';
		echo '<label for="' . $this->get_field_name( 'player_height' ) . '">';
		echo 'Width:';
		echo '<input class="widefat" id="' . $this->get_field_id( 'player_height' ) . '" name="' . $this->get_field_name( 'player_height' ) . '" type="text" value="' . esc_attr( $instance['player_height'] ) . '">';
		echo '</label>';
		echo '</p>';

	}

	/**
	 * Update Widget: Save our instance
	 *
	 * @param array $new_instance
	 * @param array $old_instance
	 *
	 * @return array
	 */
	public function update( $new_instance, $old_instance ) {

		$instance = $old_instance;

		$instance['title']         = $new_instance['title'];        // Title
		$instance['stream_id']     = $new_instance['stream_id'];    // Stream ID
		$instance['player_width']  = $new_instance['player_width'];    // Stream ID
		$instance['player_height'] = $new_instance['player_height'];    // Stream ID

		return $instance;
	}

}